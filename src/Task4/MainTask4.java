package task4;

import shared.SDANumber;

import java.util.Scanner;

class MainTask4 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        int number = scanner.nextInt();

        for (int i = 1; i <= number; i++) {
            var currentNumber = new SDANumber(i);
            if (currentNumber.isDivisibleBy(3) && currentNumber.isDivisibleBy(7)) {
                System.out.println("pif paf");
            } else if (currentNumber.isDivisibleBy(3)) {
                System.out.println("pif");
            } else if (currentNumber.isDivisibleBy(7)) {
                System.out.println("paf");
            } else {
                System.out.println(i);
            }
        }
    }
}
