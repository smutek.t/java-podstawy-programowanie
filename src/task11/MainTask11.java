package task11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class MainTask11 {

    public static void main(String[] args) throws IOException {
        var bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputText;

        //tutaj przechowamy najdłuższy tekst
        String theLongestText = "";

        //Wykonujemy pętlę, dopóki wpisany tekst jest inny niż "starczy" (nie patrząc na wielkość liter)
        String exitText = "Starczy";
        do {
            System.out.print("Enter a text: ");
            inputText = bufferedReader.readLine();

            if (inputText.isBlank()) {
                System.out.println("Text was not provided!");
                return;
            }

            int currentTextLength = inputText.length();
            //Nie bierzemy pod uwage samego teksty "starczy" - stąd ten pierwszy warunek w "if"
            //Jeśli się okaże, że nowy wprowadzony tekst jest dłuższy niż poprzedni najdłuższy, to wtedy ten nowy staje się najdłuższym tekstem
            if (!inputText.equalsIgnoreCase(exitText) && currentTextLength > theLongestText.length()) {
                theLongestText = inputText;
            }
        } while (!inputText.equalsIgnoreCase(exitText));

        //Musimy tutaj sprawdzić ten warunek, bo może się tak zdarzyć, że od razu wpiszemy "starczy" i wtedy program wypisałby pustą wartość dla najdłuższego tekstu.
        if (inputText.equalsIgnoreCase(exitText) && theLongestText.isBlank()) {
            return;
        }

        System.out.println(String.format("The longest text was: %s, its length was %s", theLongestText, theLongestText.length()));
    }
}
