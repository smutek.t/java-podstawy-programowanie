package task13;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

class MainTask13 {

    public static void main(String[] args) throws IOException {
        var bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a text: ");
        String inputText = bufferedReader.readLine();

        String[] words = inputText.split(" ");
        Random random;
        int repetition;
        var result = new StringBuilder();

        for (String word : words) {
            random = new Random();
            repetition = random.nextInt(1, 5); //(1-5) oznacza, że chcemy losować pomiędzy 1 (włączając) a 5 (wyłączając)
            for (int i = 0; i < repetition; i++) {
                result.append(word).append(" ");
            }
        }

        System.out.println(result);
    }
}
