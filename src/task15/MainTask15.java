package task15;

import java.util.Scanner;

class MainTask15 {

    public static void main(String[] args) {
        int[] numbers = getUserInput();

        for (int i = 0; i < numbers.length; i++) {
            int number = numbers[i];
            int occurrences = getNumberOccurrences(number, numbers);
            boolean wasNumberNotCheckedYet = !wasNumberAlreadyChecked(number, i, numbers);
            if (wasNumberNotCheckedYet && occurrences >= 2) {
                //mamy wypisać tutaj liczby, które wystąpiły przynajmniej 2 razy, czyli jak spełnimy warunek "occurrences >= 2"
                //...ale... jak mamy taki ciąg [2,3,4,2,,6,7,5,2] no to liczba 2 zostałaby wyświetlona tu 3 razy,
                //więc żeby nie wyświetlać tej informacji 3 razy, musi być spełniony warunek "wasNumberNotCheckedYet"
                System.out.println(number);
            }
        }
    }

    //metoda która mówi czy dana liczba już była sprawdzana pod względem liczby wystąpień (bo po co sprawdzać wystąpienie danej liczby więcej niz raz)
    //żeby sobie zaoszczędzić czasu, nie ma sensu sprawdzać od początku, tylko od miejsca, w kótrym obecnie jest ta głąwna pętla
    private static boolean wasNumberAlreadyChecked(int number, int currentNumberIndex, int[] numbers) {
        for (int i = currentNumberIndex + 1; i < numbers.length; i++) {
            if (number == numbers[i]) {
                //żeby jeszcze zaoszczędzić sobie czasu, wystarczy, że dostaniemy info, że liczba wystąpiła przynajmniej 2 razy
                //bo jak wystąpiła 3 razy albo 4 albo 5 razy, to nadal oznacza to samo, że wystąpiła przynajmniej 2 razy
                //i nie ma sensu iść aż do końca pętli mimo, że tam są jej kolejne wystąpienia
                return true;
            }
        }
        //jeżeli ten if w pętli się nie wykona, to znaczy, że liczba nie wystąpiła - czyli zwracamy `false`
        return false;
    }

    //metoda która sprawdza ile razy wystąpiła dana liczba
    private static int getNumberOccurrences(int number, int[] numbers) {
        int counter = 0;
        for (int currentNumber : numbers) {
            if (currentNumber == number) {
                counter++;
            }
        }
        return counter;
    }

    //Pobieramy 10 liczb używając do-while - dane trzymamy w tablicy
    private static int[] getUserInput() {
        var scanner = new Scanner(System.in);
        int[] numbers = new int[10];
        int i = 0;
        do {
            System.out.print("Enter a number: ");
            numbers[i] = scanner.nextInt();
            i++;
        } while (i < 10);
        return numbers;
    }
}
