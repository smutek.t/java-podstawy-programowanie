package task17;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MainTask17 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the next class date: ");
        String dateAsString = scanner.next();

        var format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate nextClassDate = LocalDate.parse(dateAsString, format);
        LocalDate today = LocalDate.now();
        int daysLeftToNextClass = Period.between(today, nextClassDate).getDays();

        System.out.println(String.format("Number of days left to the next class: %s", daysLeftToNextClass));
    }
}
