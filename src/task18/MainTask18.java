package task18;

import java.util.Scanner;
import java.util.regex.Pattern;

public class MainTask18 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a text: ");
        var userText = scanner.nextLine();

        var pattern = Pattern.compile("a+ psik", Pattern.CASE_INSENSITIVE);
        var matcher = pattern.matcher(userText);
        boolean userSneezed = matcher.find();
        if (userSneezed) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
