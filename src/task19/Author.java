package task19;

public class Author {

    private final String surname;
    private final String nationality;


    public Author(String surname, String nationality) {
        this.surname = surname;
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return surname;
    }
}
