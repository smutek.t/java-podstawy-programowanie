package task19;

public class MainTask19 {
    public static void main(String[] args) {
        Poem[] poems = createIniData();

        MaxResult maxResult = null;
        int maxStropheNumber = 0;
        for (Poem poem : poems) {
            int currentPoemStropheNumbers = poem.getStropheNumbers();
            if (currentPoemStropheNumbers > maxStropheNumber) {
                maxStropheNumber = currentPoemStropheNumbers;
                maxResult = new MaxResult(currentPoemStropheNumbers, poem.getCreator());
            }
        }

        System.out.println(String.format("Poem with max strophe number: %s", maxResult));
    }

    private static Poem[] createIniData() {
        var authorA = new Author("Author A", "Nationality A");
        var authorB = new Author("Author B", "Nationality B");
        var authorC = new Author("Author C", "Nationality C");

        Poem[] poems = {
                new Poem(authorA, 12),
                new Poem(authorB, 7),
                new Poem(authorC, 120),
        };
        return poems;
    }
}
