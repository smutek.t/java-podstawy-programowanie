package task20;

import java.util.Random;
import java.util.Scanner;

public class MainTask20 {
    public static void main(String[] args) {
        var random = new Random();
        int randomNumber = random.nextInt(101);
        System.out.println(String.format("The random number: %s", randomNumber));
        var scanner = new Scanner(System.in);
        int userNumber;

        do {
            System.out.println("Enter the number: ");
            userNumber = scanner.nextInt();

            if (userNumber < randomNumber) {
                System.out.println("Not enough");
            }
            if (userNumber > randomNumber) {
                System.out.println("Too high");
            }

        } while (userNumber != randomNumber);

        System.out.println("Bingo!");
    }
}
